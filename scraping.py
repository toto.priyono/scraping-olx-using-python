#====================================
# Author	: Toto Priyono
# Scraping Data Pricing Product at OLX.co.id
# Jogja, 24 Jul 2021
#====================================

import requests
import csv
import mysql.connector
import time
from bs4 import BeautifulSoup
from time import sleep
from datetime import date

startProg=time.time()
baseURLOLX="http://www.olx.co.id"
baseURLCity="https://www.olx.co.id/jakarta-dki_g2000007/"
tglHariIni=date.today()

myDB=mysql.connector.connect(
	host="localhost",
	user="root",
	passwd="",
	database="tokogadaidev"
	)

myCursor=myDB.cursor(buffered=True)
qDropTable="DROP TABLE IF EXISTS scraping_olx;"
qCreateDB="CREATE TABLE scraping_olx( id_scrap INT NOT NULL AUTO_INCREMENT, date_scrap DATE, type_product_scrap VARCHAR(50), brand_product_scrap VARCHAR(100), title_product_scrap VARCHAR(200), city_scrap VARCHAR(30), price_scrap INT NULL, desc_scrap TEXT, PRIMARY KEY (id_scrap) );"
print("\nScraping Engine Start....\n")

print("Drop Table...")
myCursor.execute(qDropTable)

print("Create Table...")
myCursor.execute(qCreateDB)

print("Prepearing Brand & Type Product...")
qTypeProduct="SELECT a.nama,b.nama_jenis_barang FROM barang AS a JOIN jenis_barang AS b ON a.jenis_barang_id=b.id";
myCursor.execute(qTypeProduct)
brandProduct=[]
for data in myCursor:
	tempBrand=[]
	tempBrand.append(data[0])
	tempBrand.append(data[1])
	brandProduct.append(tempBrand)

s="INSERT INTO scraping_olx (date_scrap,title_product_scrap,brand_product_scrap,type_product_scrap,city_scrap,price_scrap,desc_scrap) VALUE(%s,%s,%s,%s,%s,%s,%s)"
counter=0
for merk in brandProduct:
	counter+=1
	qMerk="q-"+merk[0].replace(" ","-");
	link=baseURLCity+qMerk
	print("Open link :",link)
	headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36'}
	page = requests.get(link,headers=headers)
	
	print("Proses scraping...")
	sp= BeautifulSoup(page.content, 'html.parser')
	list_products = sp.find_all("li",{"class" : "EIR5N"})
	for product in list_products:
		title_price=[]
		title = product.find("span",{"data-aut-id" : "itemTitle"})
		price = product.find("span",{"data-aut-id" : "itemPrice"})
		title_price.append(tglHariIni)
		title_price.append(title.text)
		title_price.append(merk[0])
		title_price.append(merk[1])
		title_price.append("Jakarta")
		try:
			title_price.append(price.text.replace(".","").replace("Rp",""))
		except AttributeError:
			title_price.append("0")			
		detail_url = product.find("a").get('href')
		detail_url =baseURLOLX+detail_url
		print("Trying access detail...",detail_url)
		try:
			page_detail=requests.get(detail_url,headers=headers)
			sp_detail=BeautifulSoup(page_detail.content,'html.parser')
			try:
				detail_product=sp_detail.find("div",{"data-aut-id":"itemDescriptionContent"}).get_text()
				print("Process get detail...")
			except AttributeError:
				print("Ups We get Empty data detail...")
				detail_product=""			
		except ValueError:
			print('Some Error occured!')
		title_price.append(detail_product)
		try:
			print("Insert Table ...")
			myCursor.execute(s,tuple(title_price))
		except mysql.connector.errors.DataError:
			print("Data Error")
		#dataFinal.append(title_price)

print("Insert to Database ...")
s="INSERT INTO scraping_olx (date_scrap,title_product_scrap,brand_product_scrap,type_product_scrap,city_scrap,price_scrap,desc_scrap) VALUE(%s,%s,%s,%s,%s,%s,%s)"

print(str(counter)+" data has been proccessed..")
endProg=time.time()
durationProg=endProg-startProg
print("Duration :",str(durationProg))
print("\nScraping Engine Stop....\n")